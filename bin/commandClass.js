var method = Command.prototype;

function Command (content,conf,intent,dueVal,dueDate,dueTime,agenda,contact){
		this._content = content;
		this._confidence = conf;
		this._intent = intent;
		this._due_val = dueVal;
		this._due_date = dueDate;
		this._due_time = dueTime;
		this._agenda = agenda;
		this._contact = contact;
};


module.exports = Command;