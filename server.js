/*

  By Vd Thatte - Created Dec 28th 2015

  The purpose of this server is:

    1. Recieve a message from the ios app

    2. Send the message to wit.ai and get a json response

    3. Parse the json response into respective fields

    4. Query events array from the user 
 
      Note: 
 
      Until we create user accounts the events array is queried 
      from a single user - object ID: ""

    5. Create an event object from the fields obtained above

      Note: the event object is just an array containing

    - text message
    - confidence value
    - intent
    - datetime
    - agenda
    - contact

    6. append the event object into the events array

    7. save the new events array into parse

    8. END

*/

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var wit = require('node-wit');
var Parse = require('parse/node').Parse;
var Command = require('./bin/commandClass.js');

var port = process.env.PORT || 1337;

// wit.ai token
var ACCESS_TOKEN = "DJLFEBRSYGLSACBFJJ426TEQGQ56GCV6";

     // PARSE ACCOUNT CREDENTIALS
var parse_JSKey = "m9KZsHg1UZhfFKluVe59wbIPrXmcSeRrU6nSDTry";
var parse_AppKey = "ra6ZjFI3LnemnLv0H661xBledswk1jYU4DogOhAn";
      // initialize 
Parse.initialize(parse_AppKey, parse_JSKey);

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function(req, res) {
  res.send('hello world');
});

app.get('/test',function(req,res){
    res.send('hello');
});



app.use(express.static('public'));

var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.post('/process_post', urlencodedParser, function (req, res) {

   // Prepare output in JSON format
   response = {
       message:req.query.message
   };

   console.log(response);
   res.send(JSON.stringify(response));
});

// The app makes a get request to the url below
app.get('/process_get', function (req, res) {

  // respond to the app with this message
  var response_message = "connection established";

   // Prepare output in JSON format
   response = {
       message:req.query.message
   }

      // All the variables below are  the various data you get from wit.ai after NLP
      var text_content = "unknown";
      var confidence_value = "unknown"; 
      var intent = "unknown";
      var due_val = "unknown";
      var due_date = "unknown";  
      var due_time = "unknown";      
      var agenda = "unknown";
      var contact = "unknown";


   // WIT SHIT - save all the data from wit here
  wit.captureTextIntent(ACCESS_TOKEN, req.query.message, function (err, res) {
      
      if (err) console.log("Error: ", err);

      var lisa_response = "";
      // parsing the JSON data
      var witresponse = JSON.stringify(res, null, " ");
      var obj = JSON.parse(witresponse);


      // save the _text from wit to text_content 
      text_content = obj._text;


      // save the confidence from wit to confidence_value
      confidence_value = obj.outcomes[0].confidence;
      if( confidence_value < 0.50 ){
        lisa_response = "I don't think I can understand that.";
      }

      // save the intent from wit to intent
      intent = obj.outcomes[0].intent;
      

      // save the datetime frm wit into due_val
      if(obj.outcomes[0].entities.datetime != undefined){

        due_val = obj.outcomes[0].entities.datetime[0].value;
        
        // process due date and due time from due_val

        due_date = "";  
        due_time = ""; 

      }

      else{

        lisa_response = "Sweet! what time do you want me to remind you?";

      }


      // save the agenda from wit into agenda
      if(obj.outcomes[0].entities.agenda_entry != undefined){

        agenda = obj.outcomes[0].entities.agenda_entry[0].value;
        lisa_response = "Sure thing! I'll remind you to " + agenda;

      }

      // save the contact from wit into contact
      if(obj.outcomes[0].entities.contact != undefined){

        contact = obj.outcomes[0].entities.contact[0].value;

      }

      //"ra6ZjFI3LnemnLv0H661xBledswk1jYU4DogOhAn", "m9KZsHg1UZhfFKluVe59wbIPrXmcSeRrU6nSDTry"

      // DEBUGGING
      // console.log("VALUES: ");
      // console.log("text:");
      // console.log(text_content);
      // console.log("confidence:");
      // console.log(confidence_value);
      // console.log("intent:");
      // console.log(intent);
      // console.log("due_val:");
      // console.log(due_val);
      // console.log("agenda:");
      // console.log(agenda);
      // console.log("contact:");
      // console.log(contact);


      //
      // API KEY:
      // "ra6ZjFI3LnemnLv0H661xBledswk1jYU4DogOhAn"
      //
      // JS KEY:
      // "m9KZsHg1UZhfFKluVe59wbIPrXmcSeRrU6nSDTry"
      //

 

      var Test = Parse.Object.extend("testClass");
      var test = new Test();
      var query = new Parse.Query(Test);
      var events = [];

      // get events array from object ID - 'yym6OMtOKW' from class named 'testClass'
      query.get("yym6OMtOKW", {
        success: function(test) {

          // The object was retrieved successfully.
          console.log("parse query success")
        
          // get events array from parse db
          events = test.get("events");
        
          // DEBUGGING
          // console.log("PARSE OBJECT: ");
          // console.log(events);

          // prepare event object
          var event = []; // event object which is an array idk how to implementa  dictionary
          event.push(text_content); // text content at position 0
          event.push(confidence_value); // confidence value at position 1
          event.push(intent); // intent at position 2
          event.push(due_val); // due date + time at position 3
          event.push(agenda); // agenda at position 4
          event.push(contact); // contact at position  5

          // push the event object to events array 
          events.push(event);

          // save the newly created events array on Parse
          test.set("recent_user_message", text_content);
          test.set("recent_lisa_response", lisa_response);
          test.set("events", events); 
          test.save();



          Parse.Push.send({
            channels: ["ios"],
            data: {
              alert: "Hello from the other sideeee"
            }
          }, {
            success: function() {
              // Push was successful
            },
            error: function(error) {
              // Handle error
            }
          });




        }, // success
        error: function(object, error) {

          // The object was not retrieved successfully.
          // error is a Parse.Error with an error code and message.
          console.log("-Parse Error-")
          console.log(Parse.Error)

        } // error ends

      }); // query.get ends

    }); // wit.capture ends
  console.log(response_message);
  res.send(response_message); // send response to the app
}) // app.get ends





/*Querying Parse for data, storing data locally.*/

var User = Parse.Object.extend("command_stack");
var query = new Parse.Query(User);

query.find({
  success: function(res){
    console.log(res.length);
    for(var i = 0; i < res.length; ++i){
      var object = res[i];
      var eventTime= object.get('Event_Execute_Date');
      console.log(object.id + ' : ' + eventTime);
      var currentTime = new Date();
      if(eventTime <= currentTime){
        console.log("reminder needs to be sent");
        /**SEND REMINDER HERE**/
      }
    }
  },
  error: function(err){
    console.log("err!");
  }
});

var parseTime = function(time){
  console.log(time);
  var date;
  var time; 
  return;
}
// get request
app.post('/response', urlencodedParser, function (req, res) {
  res.send();
}) // app.post

app.listen(port); 

